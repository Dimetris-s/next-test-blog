import styled, { keyframes } from "styled-components";

const rotate = keyframes`
    from {
        transform: translateX(-50%) rotate(0deg);
    }
    to {
        transform: translateX(-50%) rotate(360deg);
    }
`;

const Wrapper = styled.div`
	width: 0;
	height: 0;
	border: 45px solid transparent;
	border-bottom-color: ${({ theme }) => theme.palette.blue.text};
	border-top-color: ${({ theme }) => theme.palette.blue.text};
	border-radius: 50%;
	margin-top: 4rem;
	position: relative;
	left: 50%;
	transform: translateX(-50%);
	animation: ${rotate} 0.7s linear infinite;
`;
const Loader = () => <Wrapper />;

export default Loader;
