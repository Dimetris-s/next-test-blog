import React, { useEffect, useState } from "react";
import TextField from "./textField";
import styled from "styled-components";
import { validator } from "../../utils/validator";

const Form = styled.form`
	padding: 30px;
	border-radius: 15px;
	background-color: #fff;
	width: min(500px, 70vw);
	display: flex;
	flex-direction: column;
	align-items: center;
`;

const SubmitBtn = styled.button`
	background-color: ${({ theme }) => theme.palette.blue.btn};
	padding: 10px 12px;
	border-radius: 10px;
	color: ${({ theme }) => theme.palette.white.primary};
	display: flex;
	align-items: center;
	justify-content: center;
	cursor: pointer;
	font-weight: 300;
	width: 140px;
	margin-top: 15px;
`;

const CreateForm = ({ onSubmit }) => {
	const config = {
		title: {
			isRequred: { message: "Поле не может быть пустым" },
			maxLength: { value: 30, message: "Максимально допустимое число символов 30" },
		},
		text: {
			isRequred: { message: "Поле не может быть пустым" },
		},
		image: {
			isRequred: { message: "Поле не может быть пустым" },
			isLink: { message: "Ссылка указана некорректно" },
		},
	};
	const [data, setData] = useState({ title: "", text: "", image: "" });
	const [errors, setErrors] = useState({});
	const [touched, setTouched] = useState(false);
	useEffect(() => {
		if (touched) {
			validate();
		}
	}, [data]);
	const submitHandler = event => {
		event.preventDefault();
		setTouched(true);
		const isValid = validate();
		if (!isValid) return;
		onSubmit(data);
	};
	const validate = () => {
		const errors = validator(data, config);
		setErrors(errors);
		return Object.keys(errors).length === 0;
	};
	const changeHandler = ({ name, value }) => {
		setData(prevState => ({ ...prevState, [name]: value }));
	};
	return (
		<Form onSubmit={submitHandler}>
			<TextField
				error={errors.title}
				value={data.title}
				onChange={changeHandler}
				label="Название статьи:"
				name="title"
			/>
			<TextField
				error={errors.text}
				value={data.text}
				onChange={changeHandler}
				label="Текст статьи:"
				name="text"
				textarea
				rows={10}
			/>
			<TextField
				error={errors.image}
				value={data.image}
				onChange={changeHandler}
				label="URL картинки:"
				name="image"
			/>
			<SubmitBtn>Добавить</SubmitBtn>
		</Form>
	);
};

export default CreateForm;
