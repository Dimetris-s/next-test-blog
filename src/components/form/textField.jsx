import React from "react";
import styled from "styled-components";

const Wrapper = styled.div`
	display: flex;
	flex-direction: column;
	gap: 5px;
	margin-bottom: 15px;
	width: 100%;
	${({ error }) =>
		error &&
		`& input, & textarea {
        color: red;
        border-color: red;
        &:focus {
		box-shadow: 0 0 0px 4px #f1452688;
        }
    }
        & label {
            color: red;
        }
    `}
`;

const Label = styled.label`
	font-weight: 300;
	font-size: 18px;
	line-height: 21px;
	color: ${({ theme }) => theme.palette.black.text};
`;

const Input = styled.input`
	border: 1px solid #e5e5e5;
	border-radius: 5px;
	transition: all 0.3s ease;
	padding: 0.6rem;
	width: 100%;
	resize: none;
	overflow-y: auto;
	&:focus {
		outline: none;
		box-shadow: 0 0 0px 4px #26a0f189;
	}
`;

const Error = styled.p`
	color: red;
	font-size: 12px;
`;

const TextField = ({ value, onChange, label, error, name, ...rest }) => {
	const changeHandler = ({ target }) => {
		const { name, value } = target;
		onChange({ name, value });
	};
	return (
		<Wrapper error={error}>
			<Label htmlFor={name}>{label}</Label>
			<Input
				as={rest.textarea ? "textarea" : "input"}
				rows={rest.rows}
				name={name}
				id={name}
				value={value || ""}
				onChange={changeHandler}
				type="text"
			/>
			{error && <Error>{error}</Error>}
		</Wrapper>
	);
};

export default TextField;
