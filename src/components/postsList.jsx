import PostCard from "./postCard";
import styled from "styled-components";
import Loader from "./loader";
import postService from "../services/post.service";

const Wrapper = styled.ul`
	list-style: none;
	display: flex;
	gap: 50px 30px;
	flex-wrap: wrap;
`;

const PostsList = ({ posts }) => {
	return (
		<Wrapper>
			{posts.map(post => (
				<PostCard key={post._id} post={post} />
			))}
		</Wrapper>
	);
};

export default PostsList;
