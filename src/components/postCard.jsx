import Link from "next/link";
import styled from "styled-components";

const Wrapper = styled.li`
	width: clamp(300px, 25vw, 350px);
	overflow: hidden;
	border-radius: 15px;
	display: flex;
	flex-direction: column;
	cursor: pointer;
	transition: box-shadow 0.4s ease;

	&:hover img {
		transition: transform 5s linear;
		transform: scale(1.2);
	}
	&:hover {
		box-shadow: 0px 1px 8px 1px #ccc;
	}
`;

const CardImage = styled.div`
	flex: 1;
	overflow: hidden;
	img {
		width: 100%;
		height: 220px;
		display: block;
		object-fit: cover;
	}
`;

const CardTitle = styled.div`
	flex: 0.2;
	background-color: ${({ theme }) => theme.palette.white.secondary};
	padding: 15px 20px;
	display: flex;
	align-items: center;
	color: ${({ theme }) => theme.palette.blue.text};
`;
const PostCard = ({ post }) => {
	return (
		<Link href={`/post/[id]`} as={`/post/${post._id}`}>
			<Wrapper>
				<CardImage>
					<img src={post.image} />
				</CardImage>
				<CardTitle>
					<h3>{post.title}</h3>
				</CardTitle>
			</Wrapper>
		</Link>
	);
};

export default PostCard;
