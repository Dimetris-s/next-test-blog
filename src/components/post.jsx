import styled from "styled-components";

const Wrapper = styled.div`
	margin-top: 50px;
	padding: 35px;
	background-color: ${({ theme }) => theme.palette.white.secondary};
	border-radius: 15px;
	position: relative;
`;

const Title = styled.h2`
	color: ${({ theme }) => theme.palette.blue.text};
	font-weight: 500;
	font-size: 24px;
	line-height: 28px;
	margin-bottom: 25px;
`;

const Content = styled.div`
	display: flex;
	align-items: flex-start;
	gap: 25px;
`;

const Left = styled.div`
	flex: 0.8;
`;
const Text = styled.p`
	line-height: 21px;
	font-size: 18px;
	font-weight: 300;
`;
const Right = styled.div`
	flex: 1;
	width: 100%;
	img {
		width: 100%;
		max-height: 550px;
		object-fit: cover;
		display: block;
	}
`;

const DeleteBtn = styled.button`
	background-color: ${({ theme }) => theme.palette.red.btn};
	padding: 5px 12px;
	border-radius: 10px;
	color: ${({ theme }) => theme.palette.white.primary};
	display: flex;
	align-items: center;
	justify-content: center;
	position: absolute;
	left: 50%;
	transform: translateX(-50%);
	bottom: -14.5px;
	cursor: pointer;
	font-weight: 300;
`;

const Post = ({ _id, title, text, image, onDelete }) => {
	return (
		<Wrapper>
			<Title>{title}</Title>
			<Content>
				<Left>
					<Text>{text}</Text>
				</Left>
				<Right>
					<img src={image} alt="nature" />
				</Right>
			</Content>
			<DeleteBtn onClick={() => onDelete(_id)}>Удалить статью</DeleteBtn>
		</Wrapper>
	);
};

export default Post;
