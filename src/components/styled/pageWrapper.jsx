import React from "react";
import styled from "styled-components";

const Wrapper = styled.div`
	background-color: ${({ theme }) => theme.palette.blue.bg};
	min-height: 100vh;
	padding-top: 30px;
`;

const PageWrapper = ({ children }) => {
	return (
		<Wrapper>
			<div className="container">{children}</div>
		</Wrapper>
	);
};

export default PageWrapper;
