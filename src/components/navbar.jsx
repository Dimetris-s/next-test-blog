import styled from "styled-components";
import Link from "next/link";

const Nav = styled.nav`
	min-height: 55px;
	background-color: ${({ theme }) => theme.palette.white.secondary};
	display: flex;
	align-items: center;
	padding: 17px 0;
	position: relative;
`;

const Logo = styled.a`
	font-style: normal;
	font-weight: 500;
	font-size: 18px;
	line-height: 21px;
	cursor: pointer;
	color: ${({ theme }) => theme.palette.blue.text};
`;

const AddBtn = styled.a`
	background-color: ${({ theme }) => theme.palette.blue.btn};
	padding: 5px 12px;
	border-radius: 10px;
	color: ${({ theme }) => theme.palette.white.primary};
	display: flex;
	align-items: center;
	justify-content: center;
	position: absolute;
	left: 50%;
	transform: translateX(-50%);
	bottom: -14.5px;
	cursor: pointer;
	font-weight: 300;
`;

const Navbar = () => {
	return (
		<Nav>
			<div className="container">
				<Link href="/">
					<Logo>NEXT | BLOG</Logo>
				</Link>
				<Link href="/create">
					<AddBtn color="blue">Добавить статью</AddBtn>
				</Link>
			</div>
		</Nav>
	);
};

export default Navbar;
