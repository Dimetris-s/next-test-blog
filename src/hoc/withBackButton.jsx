import Link from "next/link";
import styled from "styled-components";
import arrowSVG from "../assets/arrow.svg";

const BackButton = styled.a`
	background-color: ${({ theme }) => theme.palette.white.primary};
	box-shadow: 0px 10px 25px rgba(148, 174, 213, 0.15);
	border-radius: 10px;
	padding: 10px 15px;
	color: ${({ theme }) => theme.palette.blue.text};
	display: inline-flex;
	align-items: center;
	justify-content: center;
	gap: 10px;
	cursor: pointer;
	font-weight: 600;
	transition: all 0.3s ease;
	&:hover {
		box-shadow: 0px 5px 15px rgba(148, 174, 213, 0.671);
	}
	&::before {
		content: "";
		display: block;
		background-image: url("${arrowSVG}");
		background-position: center;
		background-repeat: no-repeat;
		background-size: contain;
		width: 24px;
		height: 15px;
	}
`;
const withBackButton = Component => props => {
	return (
		<>
			<Link href="/">
				<BackButton>Назад</BackButton>
			</Link>
			<Component {...props} />
		</>
	);
};

export default withBackButton;
