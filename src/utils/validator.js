export function validator(data, config) {
	let errors = {};

	function validate(validateMethod, data, config) {
		let statusValidate;

		switch (validateMethod) {
			case "isRequred":
				statusValidate = data.trim() === "";
				break;
			case "isLink":
				const linkRegEx =
					/https?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()@:%_\+.~#?&//=]*)/g;
				statusValidate = !linkRegEx.test(data);
				break;
			case "maxLength":
				statusValidate = data.length > config.value;
			default:
				break;
		}

		if (statusValidate) {
			return config.message;
		}
	}

	for (const fieldName in data) {
		for (const validateMethod in config[fieldName]) {
			const error = validate(validateMethod, data[fieldName], config[fieldName][validateMethod]);
			if (error && !errors[fieldName]) {
				errors[fieldName] = error;
			}
		}
	}
	return errors;
}
