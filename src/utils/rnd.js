export const rnd = (min, max) => Math.round(Math.random() * (max - min) + min);
export const rndId = () => Math.random().toString(36).substr(2, 9);
