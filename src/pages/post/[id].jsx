import withBackButton from "../../hoc/withBackButton";
import { useRouter } from "next/dist/client/router";
import postService from "../../services/post.service";
import Post from "../../components/post";
import { useEffect, useState } from "react";
import Loader from "../../components/loader";

const PostPage = ({ post: serverPost }) => {
	const { push, query } = useRouter();

	const [post, setPost] = useState(serverPost);
	useEffect(() => {
		const load = async () => {
			const post = await postService.get(query.id);
			setPost(post);
		};
		if (!serverPost) {
			load();
		}
	}, []);
	const deleteHandler = async id => {
		try {
			await postService.delete(id);
			push("/");
		} catch (error) {
			console.log(error);
		}
	};
	return <>{post ? <Post {...post} onDelete={deleteHandler} /> : <Loader />}</>;
};

// export default withBackButton(PostPage);

// export async function getServerSideProps({ query }) {
// 	const post = await postService.get(query.id);

// 	return { props: { post } };
// }

const PostPageWithButton = withBackButton(PostPage);

PostPageWithButton.getInitialProps = async ({ query, req }) => {
	if (!req) return { post: null };
	const post = await postService.get(query.id);

	return { post };
};

export default PostPageWithButton;
