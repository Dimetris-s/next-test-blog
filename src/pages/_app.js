import "../styles/global.css";
import { ThemeProvider } from "styled-components";
import { theme } from "../styles/theme";
import Navbar from "../components/navbar";
import PageWrapper from "../components/styled/pageWrapper";
function MyApp({ Component, pageProps }) {
	return (
		<ThemeProvider theme={theme}>
			<Navbar />
			<PageWrapper>
				<Component {...pageProps} />
			</PageWrapper>
		</ThemeProvider>
	);
}

export default MyApp;
