import styled from "styled-components";
import PostsList from "../components/postsList";
import postService from "../services/post.service";

const Wrapper = styled.div`
	padding-top: 20px;
`;

const Home = ({ posts }) => {
	return (
		<Wrapper>
			<PostsList posts={posts} />
		</Wrapper>
	);
};

export default Home;

export async function getServerSideProps() {
	const posts = await postService.fetchAll();
	return {
		props: { posts },
	};
}
