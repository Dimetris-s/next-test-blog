import { useRouter } from "next/dist/client/router";
import styled from "styled-components";
import CreateForm from "../components/form/createForm";
import withBackButton from "../hoc/withBackButton";
import postService from "../services/post.service";
import { rndId } from "../utils/rnd";

const Wrapper = styled.div`
	padding-top: 120px;
	display: flex;
	justify-content: center;
`;

const CreatePostPage = () => {
	const { push } = useRouter();
	const submitHandler = async data => {
		try {
			await postService.create({ id: rndId(), ...data });
			push("/");
		} catch (error) {
			console.log(error);
		}
	};
	return (
		<Wrapper>
			<CreateForm onSubmit={submitHandler} />
		</Wrapper>
	);
};

export default withBackButton(CreatePostPage);
