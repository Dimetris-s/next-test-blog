export const theme = {
	palette: {
		blue: {
			text: "#3260A1",
			bg: "#EEF5FF",
			btn: "#67BFFF",
		},
		red: {
			btn: "#EB5050",
		},
		white: {
			primary: "#fff",
			secondary: "#fefefe",
		},
		black: {
			primary: "#000",
			text: "#222",
		},
	},
};
