import httpService from "./http.service";
const postEndpoint = "post/";
export default {
	fetchAll: async () => {
		try {
			const { data } = await httpService.get(postEndpoint);
			return data;
		} catch (error) {
			console.log(error);
		}
	},
	get: async id => {
		try {
			const { data } = await httpService.get(postEndpoint + id);
			return data;
		} catch (error) {
			console.log(error);
		}
	},
	create: async post => {
		try {
			const { data } = await httpService.post(postEndpoint, post);
			return data;
		} catch (error) {
			console.log(error);
		}
	},
	delete: async id => {
		try {
			const { data } = await httpService.delete(postEndpoint + id);
			return data;
		} catch (error) {
			console.log(error);
		}
	},
};
