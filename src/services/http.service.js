import axios from "axios";

axios.defaults.baseURL = "https://next-blog-server.herokuapp.com/api/";

export default {
	get: axios.get,
	post: axios.post,
	put: axios.put,
	delete: axios.delete,
};
